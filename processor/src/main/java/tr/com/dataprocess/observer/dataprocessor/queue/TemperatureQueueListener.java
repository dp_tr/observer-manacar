package tr.com.dataprocess.observer.dataprocessor.queue;

import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.command.BaseCommand;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

/**
 * Created by scay on 04.06.2015.
 */
@Service
public class TemperatureQueueListener extends CommandQueueListener {

    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            ObjectMessage objectMessage = (ObjectMessage) message;
            try {
                BaseCommand command = (BaseCommand) objectMessage.getObject();

                command.execute(null);
            } catch (JMSException e) {
                e.printStackTrace();
            }
            System.out.println("Received: " + message);
        }
        else{
            throw new RuntimeException("Not an expected message type..");
        }
    }

    @Override
    public String getQueueName() {
        return "TEMPERATURE.QUEUE";
    }
}
