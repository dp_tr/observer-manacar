package tr.com.dataprocess.observer.dataprocessor.queue;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.command.BaseCommand;

import javax.annotation.PostConstruct;
import javax.jms.*;

/**
 * Created by scay on 18.05.2015.
 */

public abstract class CommandQueueListener implements ExceptionListener, MessageListener{

    @Autowired
    private ActiveMQConnectionFactory connectionFactory;

    public CommandQueueListener() {
        System.out.println("test");
    }

    @PostConstruct
    public void start(){
        try {

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();

            connection.setExceptionListener(this);

            // Create a Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue(getQueueName());

            // Create a MessageConsumer from the Session to the Topic or Queue
            MessageConsumer consumer = session.createConsumer(destination);
            consumer.setMessageListener(this);

            // Wait for a message
//            Message message = consumer.receive(1000);
//
//
//
//            consumer.close();
//            session.close();
//            connection.close();
        } catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
        }
    }

    public abstract String getQueueName();

    @Override
    public void onException(JMSException e) {
        e.printStackTrace();
    }

}
