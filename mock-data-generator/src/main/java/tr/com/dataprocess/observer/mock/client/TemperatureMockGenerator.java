package tr.com.dataprocess.observer.mock.client;

import com.google.gson.GsonBuilder;
import tr.com.dataprocess.observer.domain.TemperatureData;
import tr.com.dataprocess.observer.domain.TemperatureUnit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Random;

/**
 * Created by VICTOR on 30.9.2015.
 */
public class TemperatureMockGenerator {

    private static String generateData(Float temp){
        TemperatureData temperatureData = new TemperatureData();
        temperatureData.setTemperature(temp);
        temperatureData.setDate(new Date());
        temperatureData.setLocation("Mock");
        temperatureData.setTemperatureUnit(TemperatureUnit.CELCIUS);
        return new GsonBuilder().create().toJson(temperatureData);
    }

    // http://localhost:8080/RESTfulExample/json/product/post
    public static void main(String[] args) {
        try {
            while (true) {
                doPostRequest();
                Thread.sleep(3000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void doPostRequest() throws IOException {
        HttpURLConnection conn = openURLConnection();

        Random random = new Random();

        String input = generateData(new Float(random.nextInt(100)/10f));

        OutputStream os = conn.getOutputStream();
        os.write(input.getBytes());
        os.flush();

        handleResponse(conn);

        conn.disconnect();
    }

    private static void handleResponse(HttpURLConnection conn) throws IOException {
        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        String output;
        System.out.println("Output from Server :[");
        while ((output = br.readLine()) != null) {
            System.out.println(output);
        }
        System.out.println("]\n");
    }

    private static HttpURLConnection openURLConnection() throws IOException {
        URL url = new URL("http://localhost:9998/collector/sensor/temperature/generic");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        return conn;
    }

}
