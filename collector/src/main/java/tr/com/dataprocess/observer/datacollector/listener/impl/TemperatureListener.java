package tr.com.dataprocess.observer.datacollector.listener.impl;

import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.command.GenericTemperatureCommand;
import tr.com.dataprocess.observer.datacollector.listener.base.AbstractSensorListener;
import tr.com.dataprocess.observer.domain.TemperatureData;
import tr.com.dataprocess.observer.domain.TemperatureUnit;

import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Date;

/**
 * Created by scay on 18.05.2015.
 */
@Path("/sensor/temperature")
@Service
public class TemperatureListener extends AbstractSensorListener<String> {
    private static final String QUEUE_NAME = "TEMPERATURE.QUEUE";

    @POST
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    @Path("/generic")
    public String onData(String json) {
        try {
            Session session = initSession();
            MessageProducer producer = getMessageProducer(session, QUEUE_NAME);

            ObjectMessage message = session.createObjectMessage();
            message.setObject(new GenericTemperatureCommand(new GsonBuilder().create().fromJson(json, TemperatureData.class)));

            producer.send(message);

        }
        catch (Exception ex){
            ex.printStackTrace();
            return ex.getMessage();
        }

        return "Accepted!";
    }

    public static void main(String...args){
        TemperatureData temperatureData = new TemperatureData();
        temperatureData.setTemperature(18.2f);
        temperatureData.setDate(new Date());
        temperatureData.setLocation("test");
        temperatureData.setTemperatureUnit(TemperatureUnit.CELCIUS);

        System.out.println(new GsonBuilder().create().toJson(temperatureData));
    }

}
