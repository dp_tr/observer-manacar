package tr.com.dataprocess.observer.datacollector.listener.base;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import tr.com.dataprocess.observer.datacollector.server.CollectorRegistry;
import tr.com.dataprocess.observer.datacollector.server.Registry;

import javax.annotation.PostConstruct;
import javax.jms.*;
import javax.ws.rs.Path;
import java.util.Set;

/**
 * Created by scay on 28.05.2015.
 */
public abstract class AbstractSensorListener<T> implements SensorListener<T> {

    @Autowired
    private ActiveMQConnectionFactory connectionFactory;

    @Autowired
    private Registry registry;

    protected MessageProducer getMessageProducer(Session session, String queueName) throws JMSException {
        // Create the destination (Topic or Queue)
        Destination destination = session.createQueue(queueName);

        MessageProducer producer = session.createProducer(destination);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        return producer;
    }

    protected Session initSession() throws JMSException {
        Connection connection = connectionFactory.createConnection();
        connection.start();

        // Create a Session
        return connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    @Override
    @PostConstruct
    public boolean register() {
        return registry.register(this);
    }
}
