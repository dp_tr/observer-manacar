package tr.com.dataprocess.observer.datacollector.server;

import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.datacollector.listener.base.SensorListener;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by scay on 28.05.2015.
 */
@Service
public class CollectorRegistry implements Registry {
    private static Set<String> pathSet = new HashSet<String>();

    @Override
    public boolean register(SensorListener listener) {
        Path pathAnnotation = listener.getClass().getAnnotation(Path.class);
        if(pathAnnotation == null){
            return false;
        }
        return pathSet.add(pathAnnotation.value());
    }

    @Override
    public boolean deRegister(SensorListener listener) {
        return false;
    }

    @Override
    public List<SensorListener> registerAll(List<SensorListener> listeners) {
        return null;
    }

    @Override
    public List<SensorListener> deRegisterAll() {
        return null;
    }

    @Override
    public List<String> getAll() {
        return new ArrayList<String>(pathSet);
    }
}
