package tr.com.dataprocess.observer.datacollector.listener.base;

import tr.com.dataprocess.observer.datacollector.server.Registry;

import java.util.Set;

/**
 * Created by scay on 18.05.2015.
 */
public interface SensorListener<T> {
    public String onData(T t);

    public boolean register();
}
