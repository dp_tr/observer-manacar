package tr.com.dataprocess.observer.datacollector.server;

import tr.com.dataprocess.observer.datacollector.listener.base.SensorListener;

import java.util.List;

/**
 * Created by scay on 28.05.2015.
 */
public interface Registry {
    boolean register(SensorListener listener);
    boolean deRegister(SensorListener listener);

    List<SensorListener> registerAll(List<SensorListener> listeners);
    List<SensorListener> deRegisterAll();

    List<String> getAll();
}
