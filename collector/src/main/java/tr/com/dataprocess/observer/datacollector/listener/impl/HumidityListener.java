package tr.com.dataprocess.observer.datacollector.listener.impl;

import com.google.gson.GsonBuilder;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.command.GenericHumidityCommand;
import tr.com.dataprocess.observer.command.GenericTemperatureCommand;
import tr.com.dataprocess.observer.datacollector.listener.base.AbstractSensorListener;
import tr.com.dataprocess.observer.domain.HumidityData;
import tr.com.dataprocess.observer.domain.TemperatureData;

import javax.jms.*;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Date;

/**
 * Created by scay on 18.05.2015.
 */
@Path("/sensor/humidity")
@Service
public class HumidityListener extends AbstractSensorListener<String> {

    private static final String QUEUE_NAME = "HUMIDITY.QUEUE";

    @POST
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    @Path("/generic")
    public String onData(String json) {
        try {
            Session session = initSession();
            MessageProducer producer = getMessageProducer(session, QUEUE_NAME);

            ObjectMessage message = session.createObjectMessage();
            message.setObject(new GenericHumidityCommand(new GsonBuilder().create().fromJson(json, HumidityData.class)));

            producer.send(message);

        }
        catch (Exception ex){
            ex.printStackTrace();
            return ex.getMessage();
        }

        return "Accepted!";
    }

    public static void main(String...args){
        HumidityData humidityData = new HumidityData();
        humidityData.setHumidityLevel(18.2f);
        humidityData.setDate(new Date());
        humidityData.setLocation("test");

        System.out.println(new GsonBuilder().create().toJson(humidityData));
    }
}
