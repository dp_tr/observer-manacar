package tr.com.dataprocess.observer.datacollector.server;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * Created by scay on 28.05.2015.
 */
public interface CollectorBase {
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    String welcome();
}
