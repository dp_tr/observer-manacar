package tr.com.dataprocess.observer.datacollector.listener.impl;

import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.command.GenericHumidityCommand;
import tr.com.dataprocess.observer.command.GenericPressureCommand;
import tr.com.dataprocess.observer.datacollector.listener.base.AbstractSensorListener;
import tr.com.dataprocess.observer.domain.HumidityData;
import tr.com.dataprocess.observer.domain.PressureData;
import tr.com.dataprocess.observer.domain.PressureUnit;

import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Date;

/**
 * Created by scay on 18.05.2015.
 */
@Path("/sensor/pressure")
@Service
public class PressureListener extends AbstractSensorListener<String> {

    private static final String QUEUE_NAME = "PRESSURE.QUEUE";

    @POST
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    @Path("/generic")
    public String onData(String json) {
        try {
            Session session = initSession();
            MessageProducer producer = getMessageProducer(session, QUEUE_NAME);

            ObjectMessage message = session.createObjectMessage();
            message.setObject(new GenericPressureCommand(new GsonBuilder().create().fromJson(json, PressureData.class)));

            producer.send(message);

        }
        catch (Exception ex){
            ex.printStackTrace();
            return ex.getMessage();
        }

        return "Accepted!";
    }

    public static void main(String...args){
        PressureData pressureData = new PressureData();
        pressureData.setValue(32f);
        pressureData.setPressureUnit(PressureUnit.BAR);
        pressureData.setDate(new Date());
        pressureData.setLocation("test");

        System.out.println(new GsonBuilder().create().toJson(pressureData));
    }
}
