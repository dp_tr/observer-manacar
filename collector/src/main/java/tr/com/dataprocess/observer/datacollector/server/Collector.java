package tr.com.dataprocess.observer.datacollector.server;

import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;
import java.io.IOException;
import java.net.URI;

/**
 * Created by scay on 28.05.2015.
 */
// The Java class will be hosted at the URI path "/collector"
@Path("/")
@Component
public class Collector implements CollectorBase{


    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:9998/collector/";

    @Autowired
    private Registry registry;

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static org.glassfish.grizzly.http.server.HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in tr.com.dataprocess package
        final ResourceConfig rc = new ResourceConfig().packages("tr.com.dataprocess");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }


    public String welcome() {
        StringBuilder stringBuilder = new StringBuilder("Welcome to Observer - Collector!").append("\n").append("Registed Listeners:").append("\n");
        for (String str : registry.getAll()){
            stringBuilder.append(str).append("\n");
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) throws IOException {
        final org.glassfish.grizzly.http.server.HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();


//        TemperatureData temperatureData = new TemperatureData(new Date(), "test", 124, TemperatureUnit.FAHRENHEIT);
//        String str = new GsonBuilder().create().toJson(temperatureData);
//        System.out.println(str);
    }
}
