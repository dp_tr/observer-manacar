package tr.com.dataprocess.observer.domain;

import org.mongodb.morphia.annotations.Entity;

import java.util.Date;

/**
 * Created by scay on 18.05.2015.
 */
@Entity
public class PressureData extends SensorData {
    static final long serialVersionUID = 1;

    private float value;
    private PressureUnit pressureUnit;

    public PressureData() {
    }

    public PressureData(PressureUnit pressureUnit, float value) {
        this.pressureUnit = pressureUnit;
        this.value = value;
    }

    public PressureData(Date date, String location, PressureUnit pressureUnit, float value) {
        super(date, location);
        this.pressureUnit = pressureUnit;
        this.value = value;
    }

    public PressureUnit getPressureUnit() {
        return pressureUnit;
    }

    public void setPressureUnit(PressureUnit pressureUnit) {
        this.pressureUnit = pressureUnit;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public static String getThresholdSettingsName() {
        return "PRESSURE_THRESHOLD";
    }

}
