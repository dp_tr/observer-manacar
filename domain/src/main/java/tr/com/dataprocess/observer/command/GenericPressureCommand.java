package tr.com.dataprocess.observer.command;

import tr.com.dataprocess.observer.domain.PressureData;
import tr.com.dataprocess.observer.domain.ThresholdSettings;
import tr.com.dataprocess.observer.utils.EmailUtils;

import java.util.List;

/**
 * Created by scay on 18.05.2015.
 */
public class GenericPressureCommand extends GenericCommand<PressureData, CommandContext> {
    static final long serialVersionUID = 1;

    public GenericPressureCommand(PressureData pressureData) {
        super(pressureData);
    }

    @Override
    public void execute(CommandContext commandContext) {
        PressureData pressureData = getEntity();
        getGenericDao().save(pressureData);

        List<ThresholdSettings> thresholdSettings = getGenericDao().findByField(ThresholdSettings.class, "name", pressureData.getThresholdSettingsName());
        if(thresholdSettings != null && thresholdSettings.size() == 1){
            ThresholdSettings thresholdSetting = thresholdSettings.get(0);
            String thresholdStr = thresholdSetting.getValue();
            Float threshold = Float.parseFloat(thresholdStr);
            if(threshold != null) {
                if (pressureData.getValue() >= threshold){
                    EmailUtils.sendEmail(thresholdSetting.getEmailTo(), thresholdSetting.getEmailCC(), thresholdSetting.getEmailSubject(), thresholdSetting.getEmailBody().replaceAll("#", thresholdStr).replaceAll("\\$", Float.toString(pressureData.getValue())));
                }
            }
        }
        System.out.println("persisted entity : " + getEntity());
    }
}
