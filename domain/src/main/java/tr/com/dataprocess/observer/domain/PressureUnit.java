package tr.com.dataprocess.observer.domain;

/**
 * Created by scay on 18.05.2015.
 */
public enum PressureUnit {
    BAR, PSI, PASCAL, AT, ATM, TORR
}
