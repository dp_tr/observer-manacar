package tr.com.dataprocess.observer.domain;

import org.mongodb.morphia.annotations.Entity;

import java.util.Date;

/**
 * Created by scay on 18.05.2015.
 */
public abstract class SensorData extends BaseEntity{
    private String location;
    private Date date;

    public SensorData() {
    }

    public SensorData(Date date, String location) {
        this.date = date;
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
