package tr.com.dataprocess.observer.domain;

import org.mongodb.morphia.annotations.Entity;

import java.util.Date;

/**
 * Created by scay on 18.05.2015.
 */
@Entity
public class TemperatureData extends SensorData{
    static final long serialVersionUID = 1;

    private float temperature;
    private TemperatureUnit temperatureUnit;

    public TemperatureData() {
    }

    public TemperatureData(Date date, String location, float temperature, TemperatureUnit temperatureUnit) {
        super(date, location);
        this.temperature = temperature;
        this.temperatureUnit = temperatureUnit;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public TemperatureUnit getTemperatureUnit() {
        return temperatureUnit;
    }

    public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
        this.temperatureUnit = temperatureUnit;
    }

    public static String getThresholdSettingsName() {
        return "TEMPERATURE_THRESHOLD";
    }
}
