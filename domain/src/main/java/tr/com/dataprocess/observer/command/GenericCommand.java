package tr.com.dataprocess.observer.command;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import tr.com.dataprocess.observer.dao.impl.GenericDao;
import tr.com.dataprocess.observer.domain.BaseEntity;

/**
 * Created by scay on 18.05.2015.
 */
@Configurable
public abstract class GenericCommand<K extends BaseEntity, T extends CommandContext> implements BaseCommand<T> {

    static final long serialVersionUID = 1;

    @Autowired
    private GenericDao<K> genericDao;

    private K k;

    public GenericCommand() {
    }

    public GenericCommand(K k) {
        this.k = k;
    }

    public abstract void execute(T commandContext);

    public K getEntity() {
        return k;
    }

    public void setK(K k) {
        this.k = k;
    }

    public GenericDao<K> getGenericDao() {
        return genericDao;
    }
}
