package tr.com.dataprocess.observer.command;

import java.io.Serializable;

/**
 * Created by scay on 18.05.2015.
 */
public interface BaseCommand<T extends CommandContext> extends Serializable{
    public void execute(T t);
}
