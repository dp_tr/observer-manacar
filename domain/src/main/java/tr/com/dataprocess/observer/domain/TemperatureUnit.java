package tr.com.dataprocess.observer.domain;

/**
 * Created by scay on 18.05.2015.
 */
public enum TemperatureUnit {
    CELCIUS, KELVIN, FAHRENHEIT
}
