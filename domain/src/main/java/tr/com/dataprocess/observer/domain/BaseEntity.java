package tr.com.dataprocess.observer.domain;

import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Version;

import java.io.Serializable;

/**
 * Created by scay on 18.05.2015.
 */
public abstract class BaseEntity implements Serializable{

    @Id
    private String id;

    @Version
    private Long version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
