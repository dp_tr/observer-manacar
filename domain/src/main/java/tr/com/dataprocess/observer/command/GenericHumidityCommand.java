package tr.com.dataprocess.observer.command;

import tr.com.dataprocess.observer.domain.HumidityData;
import tr.com.dataprocess.observer.domain.ThresholdSettings;
import tr.com.dataprocess.observer.utils.EmailUtils;

import java.util.List;

/**
 * Created by scay on 18.05.2015.
 */
public class GenericHumidityCommand extends GenericCommand<HumidityData, CommandContext>{
    static final long serialVersionUID = 1;

    public GenericHumidityCommand(HumidityData humidityData) {
        super(humidityData);
    }

    @Override
    public void execute(CommandContext commandContext) {
        HumidityData humidityData = getEntity();
        getGenericDao().save(humidityData);
        List<ThresholdSettings> thresholdSettings = getGenericDao().findByField(ThresholdSettings.class, "name", humidityData.getThresholdSettingsName());
        if(thresholdSettings != null && thresholdSettings.size() == 1){
            ThresholdSettings thresholdSetting = thresholdSettings.get(0);
            String thresholdStr = thresholdSetting.getValue();
            Float threshold = Float.parseFloat(thresholdStr);
            if(threshold != null) {
                if (humidityData.getHumidityLevel() >= threshold){
                    EmailUtils.sendEmail(thresholdSetting.getEmailTo(), thresholdSetting.getEmailCC(), thresholdSetting.getEmailSubject(), thresholdSetting.getEmailBody().replaceAll("#", thresholdStr).replaceAll("\\$", Float.toString(humidityData.getHumidityLevel())));
                }
            }
        }
        System.out.println("persisted entity : " + getEntity());
    }
}
