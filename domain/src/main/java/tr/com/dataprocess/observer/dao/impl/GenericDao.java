package tr.com.dataprocess.observer.dao.impl;

import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;
import tr.com.dataprocess.observer.dao.base.BaseDao;
import tr.com.dataprocess.observer.domain.BaseEntity;
import tr.com.dataprocess.observer.domain.TemperatureData;

import java.util.List;

/**
 * Created by scay on 03.06.2015.
 */
@Repository
public class GenericDao<T extends BaseEntity> extends BaseDao{

    public Key<T> save(T t){
        if(t.getId() != null){
            return getDatastore().merge(t);
        }
        return getDatastore().save(t);
    }

    public List<T> getAll(Class<T> clasz){
        Query query = getDatastore().createQuery(clasz);
        return query.asList();
    }

    public <K> List<K> findByField(Class<K> clasz, String fieldName, Object value){
        Query query = getDatastore().createQuery(clasz);
        query.field(fieldName).equal(value);
        return query.asList();
    }

    public <K> List<K> findByFieldGreater(Class<K> clasz, String fieldName, Object value, String orderField){
        Query query = getDatastore().createQuery(clasz);
        query.field(fieldName).greaterThan(value);
        query.order(orderField);
        return query.asList();
    }
}
