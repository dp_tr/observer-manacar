package tr.com.dataprocess.observer.command;

/**
 * Created by scay on 18.05.2015.
 */
public interface CommandContext {
    public String getParamenter(String parameterName);
}
