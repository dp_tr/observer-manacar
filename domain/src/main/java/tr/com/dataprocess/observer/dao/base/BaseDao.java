package tr.com.dataprocess.observer.dao.base;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.QueryFactory;
import tr.com.dataprocess.observer.domain.TemperatureData;
import tr.com.dataprocess.observer.domain.TemperatureUnit;

import javax.annotation.PostConstruct;
import java.net.UnknownHostException;
import java.util.Date;

/**
 * Created by scay on 28.05.2015.
 */
public abstract class BaseDao {
    private MongoClient mongoClient;
    private Datastore datastore;
    private Morphia morphia = new Morphia();

    @PostConstruct
    private void init(){
        try {
            mongoClient = new MongoClient();
            datastore = morphia.createDatastore(mongoClient, "observer");
            morphia.mapPackage("tr.com.dataprocess.observer.domain");
        }
        catch (Exception ex){
            throw new RuntimeException("Unable to connect db", ex);
        }
    }

    public Datastore getDatastore() {
        return datastore;//morphia.createDatastore(mongoClient, "observer");
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public static void main(String...args) throws UnknownHostException {

        Morphia morphia = new Morphia();
        Datastore datastore = morphia.createDatastore(new MongoClient(), "observer");
        morphia.mapPackage("tr.com.dataprocess.observer.domain");

        TemperatureData temperatureData = new TemperatureData(new Date(), "test", 67.5f, TemperatureUnit.CELCIUS);
        Key key = datastore.save(temperatureData);
        System.out.println("Key:" + key.getId());

        Query<TemperatureData> tempQuery = datastore.createQuery(TemperatureData.class);
        tempQuery.and(tempQuery.criteria("temperature").equal(67.5f), tempQuery.criteria("temperatureUnit").equal(TemperatureUnit.CELCIUS));
        TemperatureData temperatureData1 =  tempQuery.get();
        System.out.println("Id:" + temperatureData1.getId());


    }

}
