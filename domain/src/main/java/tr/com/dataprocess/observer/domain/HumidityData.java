package tr.com.dataprocess.observer.domain;

import org.mongodb.morphia.annotations.Entity;

import java.util.Date;

/**
 * Created by scay on 18.05.2015.
 */
@Entity
public class HumidityData extends SensorData {
    static final long serialVersionUID = 1;

    private float humidityLevel;



    public HumidityData() {
    }

    public HumidityData(Date date, String location, float humidityLevel) {
        super(date, location);
        this.humidityLevel = humidityLevel;
    }

    public float getHumidityLevel() {
        return humidityLevel;
    }

    public void setHumidityLevel(float humidityLevel) {
        this.humidityLevel = humidityLevel;
    }

    public static String getThresholdSettingsName() {
        return "HUMIDITY_THRESHOLD";
    }
}
