package tr.com.dataprocess.observer.command;

import org.springframework.beans.factory.annotation.Configurable;
import tr.com.dataprocess.observer.domain.TemperatureData;
import tr.com.dataprocess.observer.domain.ThresholdSettings;
import tr.com.dataprocess.observer.utils.EmailUtils;

import java.util.List;

/**
 * Created by scay on 18.05.2015.
 */
public class GenericTemperatureCommand extends GenericCommand<TemperatureData, CommandContext>{
    static final long serialVersionUID = 1;

    public GenericTemperatureCommand() {
    }

    public GenericTemperatureCommand(TemperatureData temperatureData) {
        super(temperatureData);
    }

    @Override
    public void execute(CommandContext commandContext) {
        TemperatureData temperatureData = getEntity();
        getGenericDao().save(temperatureData);

        List<ThresholdSettings> thresholdSettings = getGenericDao().findByField(ThresholdSettings.class, "name", temperatureData.getThresholdSettingsName());
        if(thresholdSettings != null && thresholdSettings.size() == 1){
            ThresholdSettings thresholdSetting = thresholdSettings.get(0);
            String thresholdStr = thresholdSetting.getValue();
            Float threshold = Float.parseFloat(thresholdStr);
            if(threshold != null) {
                if (temperatureData.getTemperature() >= threshold){
                    EmailUtils.sendEmail(thresholdSetting.getEmailTo(), thresholdSetting.getEmailCC(), thresholdSetting.getEmailSubject(), thresholdSetting.getEmailBody().replaceAll("#", thresholdStr).replaceAll("\\$", Float.toString(temperatureData.getTemperature())));
                }
            }
        }

        System.out.println("persisted entity : " + getEntity());
    }
}