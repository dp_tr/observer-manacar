package tr.com.dataprocess.observer.utils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by VICTOR on 1.8.2015.
 */
public class EmailUtils {

    private static Properties mailServerProperties;
    private static Session mailSession;
    private static MimeMessage generateMailMessage;

    static {
        setupMailServerProps();
    }

    private static void setupMailServerProps(){
        // Step1
        System.out.println("\n 1st ===> setup Mail Server Properties..");
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        System.out.println("Mail Server Properties have been setup successfully..");
    }

    private static void generateMailMessage(String to, String cc, String subject, String body) throws MessagingException {
        // Step2
        System.out.println("\n\n 2nd ===> get Mail Session..");
        mailSession = Session.getDefaultInstance(mailServerProperties, null);
        generateMailMessage = new MimeMessage(mailSession);
        generateMailMessage.addRecipients(Message.RecipientType.TO, to);
        generateMailMessage.addRecipients(Message.RecipientType.CC, cc);
        generateMailMessage.setSubject(subject);
        generateMailMessage.setContent(body, "text/html");
        System.out.println("Mail Session has been created successfully..");
    }

    public static void sendEmail(String to, String cc, String subject, String body){
        try {
            generateMailMessage(to, cc, subject, body);

            // Step3
            System.out.println("\n\n 3rd ===> Get Session and Send mail");
            Transport transport = mailSession.getTransport("smtp");

            // Enter your correct gmail UserID and Password
            // if you have 2FA enabled then provide App Specific Password
            transport.connect("smtp.gmail.com", "observerdp", "1w2e3r4t");
            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
            transport.close();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
