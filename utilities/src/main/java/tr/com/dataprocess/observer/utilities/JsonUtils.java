package tr.com.dataprocess.observer.utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by scay on 18.05.2015.
 */
public class JsonUtils {
    public static  <T> T fromJson(String jsonString, Class<T> clasz){
        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(jsonString, clasz);
    }

    public static String toJson(Object obj){
        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();
        return gson.toJson(obj);
    }
}
