package tr.com.dataprocess.observer.web.layout;

import com.google.common.eventbus.Subscribe;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Configurable;
import tr.com.dataprocess.observer.web.app.PageResolver;
import tr.com.dataprocess.observer.web.base.Event;
import tr.com.dataprocess.observer.web.utils.EventBusHandler;

/**
 * A responsive menu component providing user information and the controls for
 * primary navigation between the views.
 */
@SuppressWarnings({ "serial", "unchecked" })
@Configurable
public final class Menu extends CustomComponent {

    public static final String ID = "dashboard-menu";
    public static final String REPORTS_BADGE_ID = "dashboard-menu-reports-badge";
    public static final String NOTIFICATIONS_BADGE_ID = "dashboard-menu-notifications-badge";
    private static final String STYLE_VISIBLE = "valo-menu-visible";
    private MenuItem settingsItem;

    private PageResolver pageResolver;

    public Menu(PageResolver pageResolver) {
        this.pageResolver = pageResolver;
        setPrimaryStyleName("valo-menu");
        setId(ID);
        setSizeUndefined();
        setResponsive(true);

        // There's only one DashboardMenu per UI so this doesn't need to be
        // unregistered from the UI-scoped DashboardEventBus.
        EventBusHandler.register(this);

        setCompositionRoot(buildContent());
        Responsive.makeResponsive(this);
    }

    private Component buildContent() {
        final CssLayout menuContent = new CssLayout();
        menuContent.addStyleName("sidebar");
        menuContent.addStyleName(ValoTheme.MENU_PART);
        menuContent.addStyleName("no-vertical-drag-hints");
        menuContent.addStyleName("no-horizontal-drag-hints");
        menuContent.setWidth(null);
        menuContent.setHeight("100%");

        menuContent.addComponent(buildTitle());
//        menuContent.addComponent(buildUserMenu());
        menuContent.addComponent(buildToggleButton());
        menuContent.addComponent(buildMenuItems());
        menuContent.setResponsive(true);
        Responsive.makeResponsive(menuContent);
        return menuContent;
    }

    private Component buildTitle() {
        Label logo = new Label("Observer <strong>Presenter</strong>",
                ContentMode.HTML);
        logo.setSizeUndefined();
        HorizontalLayout logoWrapper = new HorizontalLayout(logo);
        logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
        logoWrapper.addStyleName("valo-menu-title");
        return logoWrapper;
    }

//    private User getCurrentUser() {
//        return (User) VaadinSession.getCurrent().getAttribute(
//                User.class.getName());
//    }

    private Component buildUserMenu() {
        final MenuBar settings = new MenuBar();
        settings.addStyleName("user-menu");
//        final User user = getCurrentUser();
        settingsItem = settings.addItem("", new ThemeResource(
                "img/profile-pic-300px.jpg"), null);
        updateUserName(null);
        settingsItem.addItem("Edit Profile", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
//                ProfilePreferencesWindow.open(user, false);
            }
        });
        settingsItem.addItem("Preferences", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
//                ProfilePreferencesWindow.open(user, true);
            }
        });
        settingsItem.addSeparator();
        settingsItem.addItem("Sign Out", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
//                DashboardEventBus.post(new UserLoggedOutEvent());
            }
        });
        return settings;
    }

    private Component buildToggleButton() {
        Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
                    getCompositionRoot().removeStyleName(STYLE_VISIBLE);
                } else {
                    getCompositionRoot().addStyleName(STYLE_VISIBLE);
                }
            }
        });
        valoMenuToggleButton.setIcon(FontAwesome.LIST);
        valoMenuToggleButton.addStyleName("valo-menu-toggle");
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
        return valoMenuToggleButton;
    }

    private Component buildMenuItems() {
        CssLayout menuItemsLayout = new CssLayout();
        menuItemsLayout.addStyleName("valo-menuitems");
        menuItemsLayout.setHeight(100.0f, Unit.PERCENTAGE);

        Component homeComponent = null;

        for (String menuItemName : pageResolver.getMenuItems()) {
            PageResolver.MenuItemHolder menuItemHolder = pageResolver.getMenuItem(menuItemName);
            Component menuItemComponent = new ValoMenuItemButton(menuItemHolder);

            Label menuItemBadge = new Label();
            menuItemBadge.setId(NOTIFICATIONS_BADGE_ID);
            menuItemComponent = buildBadgeWrapper(menuItemComponent, menuItemBadge);
            menuItemsLayout.addComponent(menuItemComponent);
            if(menuItemHolder.getMenuItem().home()){
                homeComponent = menuItemComponent;
            }
        }
        if(homeComponent != null){
            menuItemsLayout.removeComponent(homeComponent);
            menuItemsLayout.addComponent(homeComponent, 0);
        }
        return menuItemsLayout;

    }

    private Component buildBadgeWrapper(final Component menuItemButton,
            final Component badgeLabel) {
        CssLayout dashboardWrapper = new CssLayout(menuItemButton);
        dashboardWrapper.addStyleName("badgewrapper");
        dashboardWrapper.addStyleName(ValoTheme.MENU_ITEM);
        badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
        badgeLabel.setWidthUndefined();
        badgeLabel.setVisible(false);
        dashboardWrapper.addComponent(badgeLabel);
        return dashboardWrapper;
    }

    @Subscribe
    public void postViewChange(final tr.com.dataprocess.observer.web.base.Event.PostViewChangeEvent event) {
        // After a successful view change the menu can be hidden in mobile view.
        getCompositionRoot().removeStyleName(STYLE_VISIBLE);
    }


    @Subscribe
    public void updateUserName(final tr.com.dataprocess.observer.web.base.Event.ProfileUpdatedEvent event) {
//        User user = getCurrentUser();
        settingsItem.setText("Serif Cay");
    }

    public final class ValoMenuItemButton extends Button {

        private static final String STYLE_SELECTED = "selected";

        private final PageResolver.MenuItemHolder menuItemHolder;

        public ValoMenuItemButton(final PageResolver.MenuItemHolder menuItemHolder) {
            this.menuItemHolder = menuItemHolder;
            setPrimaryStyleName("valo-menu-item");
            setIcon(menuItemHolder.getMenuItem().icon());
            setCaption(menuItemHolder.getMenuItem().menuName().substring(0, 1).toUpperCase()
                    + menuItemHolder.getMenuItem().menuName().substring(1));
            EventBusHandler.register(this);
            addClickListener(new ClickListener() {
                @Override
                public void buttonClick(final ClickEvent event) {
                    UI.getCurrent().getNavigator()
                            .navigateTo(menuItemHolder.getMenuItem().pageName());
                }
            });

        }

        @Subscribe
        public void postViewChange(final tr.com.dataprocess.observer.web.base.Event.PostViewChangeEvent event) {
            removeStyleName(STYLE_SELECTED);
            if (event.getMenuItemHolder() != null && event.getMenuItemHolder().getMenuItem().menuName().equalsIgnoreCase(menuItemHolder.getMenuItem().menuName())) {
                addStyleName(STYLE_SELECTED);
            }
        }
    }
}
