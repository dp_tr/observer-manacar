package tr.com.dataprocess.observer.web.app;

import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.MenuItem;
import tr.com.dataprocess.observer.web.base.VaadinPage;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class PageResolver {

    private ApplicationContext applicationContext;

    private final Map<String, GenericPage> pagesMap = new HashMap<>();
    private final Map<String, MenuItemHolder> menuMap = new HashMap<>();
    private final Map<String, MenuItemHolder> pageToMenuMap = new HashMap<>();

    @Autowired
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        initPages();
        initMenu();
    }

    private synchronized void initPages(){
        Reflections reflections = new Reflections("tr.com.dataprocess.observer.web");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(VaadinPage.class, true);
        for(Class<?> pageClass : annotated){
            if(!GenericPage.class.isAssignableFrom(pageClass)){
                throw new RuntimeException("All VaadinPage annotated classes should be of type GenericPage. Found:" + pageClass);
            }
            Class<GenericPage> genericPageClass = (Class<GenericPage>) pageClass;
            VaadinPage vaadinPage = genericPageClass.getAnnotation(VaadinPage.class);
            pagesMap.put(vaadinPage.value(), applicationContext.getBean(genericPageClass));
        }
    }

    private synchronized void initMenu(){
        Reflections reflections = new Reflections("tr.com.dataprocess.observer.web");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(MenuItem.class, true);
        for(Class<?> pageClass : annotated){
            if(!GenericPage.class.isAssignableFrom(pageClass)){
                throw new RuntimeException("All VaadinPage annotated classes should be of type GenericPage. Found:" + pageClass);
            }
            Class<GenericPage> genericPageClass = (Class<GenericPage>) pageClass;
            MenuItem menuItem = genericPageClass.getAnnotation(MenuItem.class);
            GenericPage genericPage = applicationContext.getBean(genericPageClass);
            pagesMap.put(menuItem.pageName(), genericPage);
            menuMap.put(menuItem.menuName(), new MenuItemHolder(genericPage, menuItem));
            pageToMenuMap.put(menuItem.pageName(), new MenuItemHolder(genericPage, menuItem));
        }
    }

    public Set<String> getMenuItems(){
        return menuMap.keySet();
    }

    public MenuItemHolder getMenuItem(String menuName){
        return menuMap.get(menuName);
    }

    public MenuItemHolder getMenuItemFromPage(String pageName){
        return pageToMenuMap.get(pageName);
    }

    public Set<String> getPages(){
        return pagesMap.keySet();
    }

    public GenericPage getPage(String pageName){
        return pagesMap.get(pageName);
    }

    public static class MenuItemHolder{
        private MenuItem menuItem;
        private GenericPage genericPage;

        public MenuItemHolder(GenericPage genericPage, MenuItem menuItem) {
            this.genericPage = genericPage;
            this.menuItem = menuItem;
        }

        public GenericPage getGenericPage() {
            return genericPage;
        }

        public MenuItem getMenuItem() {
            return menuItem;
        }
    }


}
