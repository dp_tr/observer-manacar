package tr.com.dataprocess.observer.web.base;

import com.vaadin.navigator.View;
import com.vaadin.ui.Layout;

/**
 * Created by scay on 21.05.2015.
 */
public interface GenericPage{
    public GenericView generateView();
}
