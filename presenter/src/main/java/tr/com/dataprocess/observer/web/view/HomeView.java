package tr.com.dataprocess.observer.web.view;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.vaadin.highcharts.HighChart;
import tr.com.dataprocess.observer.web.app.MenuNavigator;
import tr.com.dataprocess.observer.web.app.PageResolver;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.layout.Menu;

/*
 * Dashboard MainView is a simple HorizontalLayout that wraps the menu on the
 * left and creates a simple container for the navigator on the right.
 */
@SuppressWarnings("serial")
public class HomeView extends HorizontalLayout implements GenericView{

    public HomeView() {
        Label label = new Label("<h1>Welcome to Observer</h1>", ContentMode.HTML);

        addComponent(label);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
