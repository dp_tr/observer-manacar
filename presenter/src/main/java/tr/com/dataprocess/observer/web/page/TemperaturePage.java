package tr.com.dataprocess.observer.web.page;

import com.vaadin.server.FontAwesome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.dao.impl.GenericDao;
import tr.com.dataprocess.observer.domain.TemperatureData;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.base.MenuItem;
import tr.com.dataprocess.observer.web.view.HumidityView;
import tr.com.dataprocess.observer.web.view.TemperatureView;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by scay on 22.05.2015.
 */

@Service
@MenuItem(menuName = "Temperature", pageName = "temperature", icon = FontAwesome.SUN_O)
public class TemperaturePage implements GenericPage {

    @Autowired
    private GenericDao genericDao;

    @Override
    public GenericView generateView() {
        java.util.Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        List<TemperatureData> temperatureDataList = genericDao.findByFieldGreater(TemperatureData.class, "date", cal.getTime(), "date");
        return new TemperatureView(temperatureDataList, genericDao);
    }
}
