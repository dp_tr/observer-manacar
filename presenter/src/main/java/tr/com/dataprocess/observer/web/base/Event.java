package tr.com.dataprocess.observer.web.base;


import tr.com.dataprocess.observer.web.app.PageResolver;

/**
 * Created by scay on 22.05.2015.
 */
public interface Event {

    public static final class UserLoginRequestedEvent implements Event{
        private final String userName, password;

        public UserLoginRequestedEvent(final String userName,
                                       final String password) {
            this.userName = userName;
            this.password = password;
        }

        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class BrowserResizeEvent implements Event{

    }

    public static class UserLoggedOutEvent implements Event{

    }

    public static class NotificationsCountUpdatedEvent implements Event{
    }

    public static final class ReportsCountUpdatedEvent implements Event{
        private final int count;

        public ReportsCountUpdatedEvent(final int count) {
            this.count = count;
        }

        public int getCount() {
            return count;
        }

    }

    public static final class PostViewChangeEvent {
        private final PageResolver.MenuItemHolder menuItemHolder;

        public PostViewChangeEvent(final PageResolver.MenuItemHolder menuItemHolder) {
            this.menuItemHolder = menuItemHolder;
        }

        public PageResolver.MenuItemHolder getMenuItemHolder() {
            return menuItemHolder;
        }
    }

    public static final class TransactionReportEvent {
//        private final Collection<Transaction> transactions;
//
//        public TransactionReportEvent(final Collection<Transaction> transactions) {
//            this.transactions = transactions;
//        }
//
//        public Collection<Transaction> getTransactions() {
//            return transactions;
//        }
    }

    public static class CloseOpenWindowsEvent implements Event{
    }

    public static class ProfileUpdatedEvent implements Event{
    }
}
