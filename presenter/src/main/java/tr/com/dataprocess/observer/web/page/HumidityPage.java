package tr.com.dataprocess.observer.web.page;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.dao.impl.GenericDao;
import tr.com.dataprocess.observer.domain.HumidityData;
import tr.com.dataprocess.observer.web.app.PageResolver;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.base.MenuItem;
import tr.com.dataprocess.observer.web.layout.MainView;
import tr.com.dataprocess.observer.web.view.HumidityView;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by scay on 22.05.2015.
 */

@Service
@MenuItem(menuName = "Humidity", pageName = "humidity", icon = FontAwesome.CLOUD)
public class HumidityPage implements GenericPage {

    @Autowired
    private GenericDao genericDao;

    @Override
    public GenericView generateView() {
        java.util.Calendar cal = new GregorianCalendar();
        cal.set(java.util.Calendar.HOUR_OF_DAY, 0);
        cal.set(java.util.Calendar.MINUTE, 0);
        List<HumidityData> humidityDataList = genericDao.findByFieldGreater(HumidityData.class, "date", cal.getTime(), "date");
        return new HumidityView(humidityDataList, genericDao);
    }
}
