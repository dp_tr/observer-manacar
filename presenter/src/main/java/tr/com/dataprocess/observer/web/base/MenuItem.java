package tr.com.dataprocess.observer.web.base;

import com.vaadin.server.FontAwesome;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by scay on 22.05.2015.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) //can use in class only.
public @interface MenuItem{
    String menuName();
    String pageName();
    FontAwesome icon();
    boolean stateful() default false;
    boolean home() default false;
}
