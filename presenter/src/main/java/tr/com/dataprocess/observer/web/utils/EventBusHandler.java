package tr.com.dataprocess.observer.web.utils;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import tr.com.dataprocess.observer.web.app.VaadinApplication;

/**
 * Created by scay on 22.05.2015.
 */

public class EventBusHandler implements SubscriberExceptionHandler {

    private final EventBus eventBus = new EventBus(this);

    public static void post(final Object event) {
        VaadinApplication.getEventbus().eventBus.post(event);
    }

    public static void register(final Object object) {
        VaadinApplication.getEventbus().eventBus.register(object);
    }

    public static void unregister(final Object object) {
        VaadinApplication.getEventbus().eventBus.unregister(object);
    }

    @Override
    public final void handleException(final Throwable exception,
                                      final SubscriberExceptionContext context) {
        exception.printStackTrace();
    }
}
