package tr.com.dataprocess.observer.web.view;

import com.vaadin.event.UIEvents;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Responsive;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import org.vaadin.highcharts.HighChart;
import tr.com.dataprocess.observer.dao.impl.GenericDao;
import tr.com.dataprocess.observer.domain.TemperatureData;
import tr.com.dataprocess.observer.domain.ThresholdSettings;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.hicharts.BasicLine;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;

/*
 * Dashboard MainView is a simple HorizontalLayout that wraps the menu on the
 * left and creates a simple container for the navigator on the right.
 */
@SuppressWarnings("serial")
public class TemperatureView extends VerticalLayout implements GenericView{

    private final HighChart chart = new HighChart();
    private Table table;
    private boolean chartVisible = true;
    private final List<TemperatureData> temperatureDataList;

    private final GenericDao genericDao;

    public TemperatureView(List<TemperatureData> tdList, final GenericDao genericDao) {
        this.genericDao = genericDao;
        this.temperatureDataList = tdList;
        Responsive.makeResponsive(this);
        setSizeFull();
//        setWidthUndefined();
//        setHeightUndefined();
        generateThresholdSettings();

        generateChart(temperatureDataList);
        UI.getCurrent().addPollListener(new UIEvents.PollListener() {
            @Override
            public void poll(UIEvents.PollEvent event) {
                java.util.Calendar cal = new GregorianCalendar();
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                List<TemperatureData> tdList = genericDao.findByFieldGreater(TemperatureData.class, "date", cal.getTime(), "date");
                if(tdList.size() != temperatureDataList.size()){
                    temperatureDataList.clear();
                    temperatureDataList.addAll(tdList);
                    if(chartVisible) {
                        generateChart(temperatureDataList);
                    }
                    else{
                        generateTable(temperatureDataList);
                    }
                }
                System.out.println("Poll...");
            }
        });
        UI.getCurrent().setPollInterval(1000);

    }

    private void generateChart(List<TemperatureData> temperatureDataList) {
        if(table != null) {
            removeComponent(table);
        }
        BasicLine basicLine = new BasicLine();
        basicLine.setTitle(new BasicLine.ChartTitle("Temperature Chart", 0));

        List<Object> temperatureSerie = new ArrayList<>();
        List<String> timeSerie = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        for(TemperatureData temperatureData : temperatureDataList){
            temperatureSerie.add(temperatureData.getTemperature());
            timeSerie.add(sdf.format(temperatureData.getDate()));
        }
        BasicLine.ChartDataSerie tempChartSerie = new BasicLine.ChartDataSerie();
        tempChartSerie.setName("Temperature");
        tempChartSerie.setData(temperatureSerie);

        BasicLine.ChartXAxis xAxis = new BasicLine.ChartXAxis(timeSerie);
        xAxis.setLabels(new BasicLine.Labels(45, 20));
        basicLine.setxAxis(xAxis);
        basicLine.setSeries(Arrays.asList(tempChartSerie));

        BasicLine.ChartOptions plotOptions = new BasicLine.ChartOptions();
        BasicLine.PlotSeries plotSeries = new BasicLine.PlotSeries();
        plotSeries.setAnimation(false);
        plotOptions.setSeries(plotSeries);
        basicLine.setPlotOptions(plotOptions);


        chart.setHcjs(basicLine.toJson());
        chart.setSizeFull();
        addComponent(chart);
//        setMargin(true);
        setComponentAlignment(chart, Alignment.MIDDLE_LEFT);
        setExpandRatio(chart, 0.9f);
        Responsive.makeResponsive(chart);
    }

    private void generateTable(List<TemperatureData> temperatureDataList) {
        if(table != null) {
            removeComponent(table);
        }
        table = new Table();
        table.setSizeFull();
        Collections.reverse(temperatureDataList);
        table.addItems(temperatureDataList);
        table.setSelectable(true);
        if(temperatureDataList.size() > 0) {
            table.select(temperatureDataList.get(0));
        }
//        table.setStyleName("login-panel");
        //add table columns
        table.addGeneratedColumn("location", new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                return new Label(((TemperatureData) itemId).getLocation());
            }
        });
        table.addGeneratedColumn("date", new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy EEEE HH:mm:ss", new Locale("TR"));
                return new Label(sdf.format(((TemperatureData) itemId).getDate()));
            }
        });
        table.addGeneratedColumn("unit", new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                return new Label( ((TemperatureData) itemId).getTemperatureUnit().toString());
            }
        });

        table.addGeneratedColumn("temperature", new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                return new Label(Float.toString(((TemperatureData) itemId).getTemperature()));
            }
        });

        table.setColumnHeaders("Censor Area", "Time", "Temperature Unit", "Temperature Value");

        addComponent(table);
//        setMargin(true);
        setComponentAlignment(table, Alignment.MIDDLE_LEFT);
        setExpandRatio(table, 0.9f);
        Responsive.makeResponsive(table);
    }


    private void generateThresholdSettings(){
        final List<ThresholdSettings> thresholdSettings = genericDao.findByField(ThresholdSettings.class, "name", TemperatureData.getThresholdSettingsName());
        ThresholdSettings thresholdSetting = null;
        if(thresholdSettings != null && thresholdSettings.size() > 0){
            thresholdSetting = thresholdSettings.get(0);
        }


        GridLayout thresholdSettingsLayout = new GridLayout(2, 6);
        thresholdSettingsLayout.setColumnExpandRatio(0, 3f);
        thresholdSettingsLayout.setColumnExpandRatio(1, 1f);
//        thresholdSettingsLayout.setColumnExpandRatio(2, 0.1f);

        Label toLabel = new Label("Email To");
        final TextField emailTo = new TextField();
        emailTo.setValue(thresholdSetting==null ? "" : thresholdSetting.getEmailTo());
        thresholdSettingsLayout.addComponent(toLabel, 0, 0 );
        thresholdSettingsLayout.addComponent(emailTo, 1, 0);
        thresholdSettingsLayout.setComponentAlignment(emailTo, Alignment.MIDDLE_RIGHT);

        Label ccLabel = new Label("Email CC");
        final TextField emailCC = new TextField();
        emailCC.setValue(thresholdSetting==null ? "" : thresholdSetting.getEmailCC());
        thresholdSettingsLayout.addComponent(ccLabel, 0, 1);
        thresholdSettingsLayout.addComponent(emailCC, 1, 1);
        thresholdSettingsLayout.setComponentAlignment(emailCC, Alignment.MIDDLE_RIGHT);


        Label subjectLabel = new Label("Email Subject");
        final TextField emailSubject = new TextField();
        emailSubject.setValue(thresholdSetting==null ? "" : thresholdSetting.getEmailSubject());
        thresholdSettingsLayout.addComponent(subjectLabel, 0, 2);
        thresholdSettingsLayout.addComponent(emailSubject, 1, 2);
        thresholdSettingsLayout.setComponentAlignment(emailSubject, Alignment.MIDDLE_RIGHT);


        Label bodyLabel = new Label("Email Body");
        final TextArea emailBody = new TextArea();
        emailBody.setValue(thresholdSetting==null ? "" : thresholdSetting.getEmailBody());
        thresholdSettingsLayout.addComponent(bodyLabel, 0, 3);
        thresholdSettingsLayout.addComponent(emailBody, 1, 3);
        thresholdSettingsLayout.setComponentAlignment(emailBody, Alignment.MIDDLE_RIGHT);

        Label thresholdLabel =  new Label("Threshold");
        final TextField threshold = new TextField();
        threshold.setValue(thresholdSetting==null ? "" : thresholdSetting.getValue());

        Button thresholdButton = new Button("Set");

        thresholdSettingsLayout.addComponent(thresholdLabel, 0, 4);
//        thresholdLayout.setComponentAlignment(thresholdLabel, Alignment.MIDDLE_LEFT);
        thresholdSettingsLayout.addComponent(threshold, 1, 4);
        thresholdSettingsLayout.setComponentAlignment(threshold, Alignment.MIDDLE_RIGHT);

        thresholdSettingsLayout.addComponent(thresholdButton, 1, 5);
        thresholdSettingsLayout.setComponentAlignment(thresholdButton, Alignment.MIDDLE_RIGHT);

        final Window thresholdWindow = new Window("Threshold Settings");
        Responsive.makeResponsive(thresholdWindow);
        thresholdWindow.setResponsive(true);
        thresholdWindow.setModal(true);
        thresholdWindow.setHeight(400, Unit.PIXELS);
        thresholdWindow.setWidth(400, Unit.PIXELS);
        thresholdSettingsLayout.setMargin(new MarginInfo(false, true, false, true));
        thresholdSettingsLayout.setSizeFull();
        Responsive.makeResponsive(thresholdSettingsLayout);
        thresholdWindow.setContent(thresholdSettingsLayout);


        thresholdButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                ThresholdSettings thresholdSetting = null;
                if(thresholdSettings != null && thresholdSettings.size() > 0){
                    thresholdSetting = thresholdSettings.get(0);
                }
                else {
                    thresholdSetting = new ThresholdSettings();
                }
                thresholdSetting.setName(TemperatureData.getThresholdSettingsName());
                thresholdSetting.setValue(threshold.getValue());
                thresholdSetting.setEmailBody(emailBody.getValue());
                thresholdSetting.setEmailCC(emailCC.getValue());
                thresholdSetting.setEmailTo(emailTo.getValue());
                thresholdSetting.setEmailSubject(emailSubject.getValue());
                genericDao.save(thresholdSetting);
                UI.getCurrent().removeWindow(thresholdWindow);
                Notification.show("Threshold value set success!");
            }
        });


        HorizontalLayout buttonLayout = new HorizontalLayout();


        Button thresholdWindowButton = new Button("Threshold Settings");
        thresholdWindowButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                UI.getCurrent().addWindow(thresholdWindow);
            }
        });

        buttonLayout.addComponent(thresholdWindowButton);
//        setComponentAlignment(thresholdWindowButton, Alignment.MIDDLE_RIGHT);


        final Button tableViewButton = new Button("Table View");
        tableViewButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if(chartVisible){
                    removeComponent(chart);
                    chartVisible = false;
                    tableViewButton.setCaption("Chart View");
                    generateTable(temperatureDataList);
                }
                else{
                    chartVisible = true;
                    tableViewButton.setCaption("Table View");
                    generateChart(temperatureDataList);
                }
            }
        });

        buttonLayout.addComponent(tableViewButton);
        addComponent(buttonLayout);

        buttonLayout.setComponentAlignment(thresholdWindowButton, Alignment.MIDDLE_RIGHT);
        buttonLayout.setComponentAlignment(tableViewButton, Alignment.MIDDLE_RIGHT);
        setComponentAlignment(buttonLayout, Alignment.MIDDLE_RIGHT);
//        setExpandRatio(thresholdWindowButton, 0.1f);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
