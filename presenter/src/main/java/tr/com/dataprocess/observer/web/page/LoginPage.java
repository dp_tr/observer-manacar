package tr.com.dataprocess.observer.web.page;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Layout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.base.VaadinPage;
import tr.com.dataprocess.observer.web.view.LoginView;

/**
 * Created by scay on 22.05.2015.
 */
@Service
@VaadinPage("login")
public class LoginPage implements GenericPage {
    @Override
    public GenericView generateView() {
        return new LoginView();
    }

//
}
