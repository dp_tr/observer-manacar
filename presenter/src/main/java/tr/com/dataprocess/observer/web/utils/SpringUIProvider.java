package tr.com.dataprocess.observer.web.utils;

import com.vaadin.server.DefaultUIProvider;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.vaadin.highcharts.demo.DemoUI;

/**
 * Created by scay on 21.05.2015.
 */
public class SpringUIProvider extends DefaultUIProvider {

    @Override
    public UI createInstance(UICreateEvent event) {
        ApplicationContext ctx =  WebApplicationContextUtils
                .getWebApplicationContext(VaadinServlet.getCurrent().getServletContext());

//        if(event.getUIClass() == DemoUI.class){
//            return new DemoUI();
//        }
        return ctx.getBean(event.getUIClass());
    }
}
