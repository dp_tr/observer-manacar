package tr.com.dataprocess.observer.web.page;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.web.app.PageResolver;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.base.MenuItem;
import tr.com.dataprocess.observer.web.layout.MainView;
import tr.com.dataprocess.observer.web.view.HomeView;

/**
 * Created by scay on 22.05.2015.
 */

@Service
@MenuItem(menuName = "Home", pageName = "home", icon = FontAwesome.HOME, home = true)
public class HomePage implements GenericPage {

    @Override
    public GenericView generateView() {
        return new HomeView();
    }

}
