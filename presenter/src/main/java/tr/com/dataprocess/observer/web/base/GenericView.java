package tr.com.dataprocess.observer.web.base;

import com.vaadin.navigator.View;
import com.vaadin.ui.Layout;

/**
 * Created by scay on 23.05.2015.
 */
public interface GenericView extends View, Layout {
}
