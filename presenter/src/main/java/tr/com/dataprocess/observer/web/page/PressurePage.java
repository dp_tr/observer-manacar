package tr.com.dataprocess.observer.web.page;

import com.vaadin.server.FontAwesome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.dao.impl.GenericDao;
import tr.com.dataprocess.observer.domain.PressureData;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.base.MenuItem;
import tr.com.dataprocess.observer.web.view.HumidityView;
import tr.com.dataprocess.observer.web.view.PressureView;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by scay on 22.05.2015.
 */

@Service
@MenuItem(menuName = "Pressure", pageName = "pressure", icon = FontAwesome.COMPRESS)
public class PressurePage implements GenericPage {

    @Autowired
    private GenericDao genericDao;

    @Override
    public GenericView generateView() {
        java.util.Calendar cal = new GregorianCalendar();
        cal.set(java.util.Calendar.HOUR_OF_DAY, 0);
        cal.set(java.util.Calendar.MINUTE, 0);
        List<PressureData> pressureDataList = genericDao.findByFieldGreater(PressureData.class, "date", cal.getTime(), "date");
        return new PressureView(pressureDataList, genericDao);
    }
}
