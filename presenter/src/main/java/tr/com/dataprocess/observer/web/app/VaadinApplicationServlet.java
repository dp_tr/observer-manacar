package tr.com.dataprocess.observer.web.app;

import com.vaadin.server.VaadinServlet;

import javax.servlet.ServletException;

/**
 * Created by scay on 22.05.2015.
 */
public class VaadinApplicationServlet extends VaadinServlet {
    @Override
    protected final void servletInitialized() throws ServletException {
        super.servletInitialized();
        getService().addSessionInitListener(new SessionInitListener());
    }
}
