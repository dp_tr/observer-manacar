package tr.com.dataprocess.observer.web.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tr.com.dataprocess.observer.web.app.PageResolver;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.MenuItem;
import tr.com.dataprocess.observer.web.base.VaadinPage;
import tr.com.dataprocess.observer.web.layout.MainView;
import tr.com.dataprocess.observer.web.utils.EventBusHandler;

/**
 * Created by scay on 22.05.2015.
 */

public class ErrorView extends Panel implements View {


    public ErrorView() {
        addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        EventBusHandler.register(this);

        VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        root.setMargin(true);
        root.addStyleName("dashboard-view");
        setContent(root);
        Responsive.makeResponsive(root);

        Component content = new Label("Error occured!");
        root.addComponent(content);
        root.setExpandRatio(content, 1);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
