package tr.com.dataprocess.observer.web.app;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.server.Responsive;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Configurable;
import org.vaadin.googleanalytics.tracking.GoogleAnalyticsTracker;
import tr.com.dataprocess.observer.web.base.Event;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.base.GenericView;
import tr.com.dataprocess.observer.web.base.MenuItem;
import tr.com.dataprocess.observer.web.page.HomePage;
import tr.com.dataprocess.observer.web.utils.EventBusHandler;
import tr.com.dataprocess.observer.web.view.ErrorView;

@SuppressWarnings("serial")
@Configurable
public class MenuNavigator extends Navigator {

    private PageResolver pageResolver;
    // Provide a Google Analytics tracker id here
    private static final String TRACKER_ID = null;// "UA-658457-6";
    private GoogleAnalyticsTracker tracker;

    private static final String ERROR_VIEW = "error";
    private ViewProvider errorViewProvider;


    public MenuNavigator(ComponentContainer container, PageResolver pageResolver) {
        super(UI.getCurrent(), container);
        this.pageResolver = pageResolver;

        String host = getUI().getPage().getLocation().getHost();
        if (TRACKER_ID != null && host.endsWith("demo.vaadin.com")) {
            initGATracker(TRACKER_ID);
        }
        initViewChangeListener();
        initViewProviders();

    }

    private void initGATracker(final String trackerId) {
        tracker = new GoogleAnalyticsTracker(trackerId, "demo.vaadin.com");

        // GoogleAnalyticsTracker is an extension add-on for UI so it is
        // initialized by calling .extend(UI)
        tracker.extend(UI.getCurrent());
    }

    private void initViewChangeListener() {
        addViewChangeListener(new ViewChangeListener() {

            @Override
            public boolean beforeViewChange(final ViewChangeEvent event) {
                // Since there's no conditions in switching between the views
                // we can always return true.
                return true;
            }

            @Override
            public void afterViewChange(final ViewChangeEvent event) {
                PageResolver.MenuItemHolder menuItemHolder = pageResolver.getMenuItemFromPage(event.getViewName());
                // Appropriate events get fired after the view is changed.
                EventBusHandler.post(new Event.PostViewChangeEvent(menuItemHolder));
                EventBusHandler.post(new Event.BrowserResizeEvent());
                EventBusHandler.post(new Event.CloseOpenWindowsEvent());

                if (tracker != null) {
                    // The view change is submitted as a pageview for GA tracker
                    tracker.trackPageview("/dashboard/" + event.getViewName());
                }
            }
        });
    }

    private void initViewProviders() {
        // A dedicated view provider is added for each separate view type
        for (String menuItemName : pageResolver.getMenuItems()) {
            final PageResolver.MenuItemHolder menuItemHolder = pageResolver.getMenuItem(menuItemName);
            ViewProvider viewProvider = new ClassBasedViewProvider(
                    menuItemHolder.getMenuItem().pageName(), GenericView.class) {

                // This field caches an already initialized view instance if the
                // view should be cached (stateful views).
                private View cachedInstance;

                @Override
                public View getView(final String viewName) {
                    View result = null;
                    if (menuItemHolder.getMenuItem().pageName().equals(viewName)) {
                        if (menuItemHolder.getMenuItem().stateful()) {
                            // Stateful views get lazily instantiated
                            if (cachedInstance == null) {
                                cachedInstance = super.getView(menuItemHolder.getMenuItem().pageName());
                            }
                            result = cachedInstance;
                        } else {
                            // Non-stateful views get instantiated every time
                            // they're navigated to
                            result = menuItemHolder.getGenericPage().generateView();
                        }
                    }
                    return result;
                }
            };

            addProvider(viewProvider);
        }

        setErrorProvider(new ViewProvider() {
            @Override
            public String getViewName(final String viewAndParameters) {
                if(viewAndParameters == null || viewAndParameters.equalsIgnoreCase("")){
                    return HomePage.class.getAnnotation(MenuItem.class).pageName();
                }
                return ERROR_VIEW;
            }

            @Override
            public View getView(final String viewName) {
                GenericPage genericPage = pageResolver.getPage(viewName);
                if(genericPage != null){
                    return genericPage.generateView();
                }
                PageResolver.MenuItemHolder menuItemHolder = pageResolver.getMenuItemFromPage(viewName);
                if(menuItemHolder != null){
                    return menuItemHolder.getGenericPage().generateView();
                }
                return new ErrorView();
            }
        });
    }
}
