package tr.com.dataprocess.observer.web.hicharts;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Created by scay on 28.05.2015.
 */
public class BasicLine {

    private ChartTitle title;
    private ChartTitle subTitle;
    private ChartXAxis xAxis;
    private ChartYAxis yAxis;
    private ChartOptions plotOptions;
    private ChartTooltip tooltip;
    private ChartLegend legend;
    private List<ChartDataSerie> series;

    public BasicLine() {
    }

    public String toJson(){
        return "var options = " + new GsonBuilder().create().toJson(this);
    }

    public ChartLegend getLegend() {
        return legend;
    }

    public void setLegend(ChartLegend legend) {
        this.legend = legend;
    }

    public List<ChartDataSerie> getSeries() {
        return series;
    }

    public void setSeries(List<ChartDataSerie> series) {
        this.series = series;
    }

    public ChartTitle getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(ChartTitle subTitle) {
        this.subTitle = subTitle;
    }

    public ChartTitle getTitle() {
        return title;
    }

    public void setTitle(ChartTitle title) {
        this.title = title;
    }

    public ChartTooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(ChartTooltip tooltip) {
        this.tooltip = tooltip;
    }

    public ChartXAxis getxAxis() {
        return xAxis;
    }

    public void setxAxis(ChartXAxis xAxis) {
        this.xAxis = xAxis;
    }

    public ChartYAxis getyAxis() {
        return yAxis;
    }

    public void setyAxis(ChartYAxis yAxis) {
        this.yAxis = yAxis;
    }

    public ChartOptions getPlotOptions() {
        return plotOptions;
    }

    public void setPlotOptions(ChartOptions plotOptions) {
        this.plotOptions = plotOptions;
    }

    public static class ChartTitle {
        private String text;
        private int x;

        public ChartTitle() {
        }

        public ChartTitle(String text, int x) {
            this.text = text;
            this.x = x;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
    }

    public static class ChartXAxis {
        private List<String> categories;

        private Labels labels;

        public ChartXAxis() {
        }

        public ChartXAxis(List<String> categories) {
            this.categories = categories;
        }

        public List<String> getCategories() {
            return categories;
        }

        public void setCategories(List<String> categories) {
            this.categories = categories;
        }

        public Labels getLabels() {
            return labels;
        }

        public void setLabels(Labels labels) {
            this.labels = labels;
        }
    }

    public static class ChartYAxis {
        private ChartTitle title;
        private List<ChartPlotLine> chartPlotLines;

        public ChartYAxis() {
        }

        public ChartYAxis(List<ChartPlotLine> chartPlotLines, ChartTitle title) {
            this.chartPlotLines = chartPlotLines;
            this.title = title;
        }

        public List<ChartPlotLine> getChartPlotLines() {
            return chartPlotLines;
        }

        public void setChartPlotLines(List<ChartPlotLine> chartPlotLines) {
            this.chartPlotLines = chartPlotLines;
        }

        public ChartTitle getTitle() {
            return title;
        }

        public void setTitle(ChartTitle title) {
            this.title = title;
        }
    }

    public static class ChartPlotLine {
        private int value = 0;
        private int width = 1;
        private String color = "#808080";

        public ChartPlotLine() {
        }

        public ChartPlotLine(String color, int value, int width) {
            this.color = color;
            this.value = value;
            this.width = width;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }
    }

    public static class ChartTooltip {
        private String valueSuffix;

        public ChartTooltip() {
        }

        public ChartTooltip(String valueSuffix) {
            this.valueSuffix = valueSuffix;
        }

        public String getValueSuffix() {
            return valueSuffix;
        }

        public void setValueSuffix(String valueSuffix) {
            this.valueSuffix = valueSuffix;
        }
    }

    public static class ChartLegend {
        private String layout = "vertical";
        private String align = "right";
        private String verticalAlign = "middle";
        private int borderWidth = 0;

        public ChartLegend() {
        }

        public ChartLegend(String align, int borderWidth, String layout, String verticalAlign) {
            this.align = align;
            this.borderWidth = borderWidth;
            this.layout = layout;
            this.verticalAlign = verticalAlign;
        }

        public String getAlign() {
            return align;
        }

        public void setAlign(String align) {
            this.align = align;
        }

        public int getBorderWidth() {
            return borderWidth;
        }

        public void setBorderWidth(int borderWidth) {
            this.borderWidth = borderWidth;
        }

        public String getLayout() {
            return layout;
        }

        public void setLayout(String layout) {
            this.layout = layout;
        }

        public String getVerticalAlign() {
            return verticalAlign;
        }

        public void setVerticalAlign(String verticalAlign) {
            this.verticalAlign = verticalAlign;
        }

    }

    public static class ChartDataSerie {
        private String name;
        private List<Object> data;

        public ChartDataSerie() {
        }

        public ChartDataSerie(List<Object> data, String name) {
            this.data = data;
            this.name = name;
        }

        public List<Object> getData() {
            return data;
        }

        public void setData(List<Object> data) {
            this.data = data;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class ChartOptions {

        private PlotSeries series;

        public PlotSeries getSeries() {
            return series;
        }

        public void setSeries(PlotSeries series) {
            this.series = series;
        }
    }

    public static class PlotSeries {
        private boolean animation = true;

        public boolean isAnimation() {
            return animation;
        }

        public void setAnimation(boolean animation) {
            this.animation = animation;
        }
    }

    public static class Labels {
        private int rotation;
        private int step;

        public Labels() {
        }

        public Labels(int rotation, int step) {
            this.rotation = rotation;
            this.step = step;
        }

        public int getRotation() {
            return rotation;
        }

        public void setRotation(int rotation) {
            this.rotation = rotation;
        }

        public int getStep() {
            return step;
        }

        public void setStep(int step) {
            this.step = step;
        }
    }
}
