package tr.com.dataprocess.observer.web.app;

import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import tr.com.dataprocess.observer.web.base.GenericPage;
import tr.com.dataprocess.observer.web.layout.MainView;
import tr.com.dataprocess.observer.web.utils.EventBusHandler;

/**
 * Created by scay on 21.05.2015.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Theme("dashboard")
@Title("Valo Theme Test")
//@PreserveOnRefresh
public class VaadinApplication extends UI{

    private final EventBusHandler eventbus = new EventBusHandler();

    @Autowired
    private PageResolver pageResolver;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        Responsive.makeResponsive(this);
        EventBusHandler.register(this);
        addStyleName(ValoTheme.UI_WITH_MENU);

        // Authenticated user
        setContent(new MainView(pageResolver));

//        forward(vaadinRequest);
    }

    private void forward(VaadinRequest vaadinRequest) {
        VaadinServletRequest vaadinServletRequest = (VaadinServletRequest) vaadinRequest;
        String requestUri = vaadinServletRequest.getRequestURI();
        String requestPage = requestUri.substring(requestUri.lastIndexOf('/')+1);
        GenericPage genericPage = pageResolver.getPage(requestPage);

        if(genericPage != null){
            setContent(genericPage.generateView());
        }
        else {
            VerticalLayout verticalLayout = new VerticalLayout();
            setContent(verticalLayout);
            verticalLayout.addComponent(new Label("Page not found! Page:" + requestUri));
        }
    }

    @Subscribe
    public void closeOpenWindows(final tr.com.dataprocess.observer.web.base.Event.CloseOpenWindowsEvent event) {
        for (Window window : getWindows()) {
            window.close();
        }
    }

    public static EventBusHandler getEventbus() {
        return ((VaadinApplication) getCurrent()).eventbus;
    }

}
