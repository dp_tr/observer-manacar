package tr.com.dataprocess.observer.web.layout;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Responsive;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import tr.com.dataprocess.observer.web.app.MenuNavigator;
import tr.com.dataprocess.observer.web.app.PageResolver;
import tr.com.dataprocess.observer.web.base.GenericView;

/*
 * Dashboard MainView is a simple HorizontalLayout that wraps the menu on the
 * left and creates a simple container for the navigator on the right.
 */
@SuppressWarnings("serial")
public class MainView extends HorizontalLayout implements GenericView{

    public MainView(PageResolver pageResolver) {
        Responsive.makeResponsive(this);
        setResponsive(true);
        setSizeFull();
        addStyleName("mainview");

        addComponent(new Menu(pageResolver));

        ComponentContainer content = new CssLayout();
        content.addStyleName("view-content");
        content.setSizeFull();
        addComponent(content);
        setExpandRatio(content, 1.0f);

        new MenuNavigator(content, pageResolver);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
